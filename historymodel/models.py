import copy
import datetime

from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.db.models.signals import post_save
from django.db.models.base import ModelBase, Model
from django.db.models.loading import get_model
from django.dispatch import receiver


def get_class(classname, modulename):
    from_module = __import__(modulename, globals(), locals(), classname)
    return getattr(from_module, classname)


class HistoryManager(models.Manager):
    """ Custom manager for HistoryModel subclasses

    This manager simply provides a date-range search method.

    """

    def all(self):
        self.order_by('timestamp').all()

    def by_date_range(self, start_date, end_date=None):
        end_date = end_date or start_date
        start_time = datetime.dateime.combine(
            start_date,
            datetime.time(0, 0, 0)
        )
        end_time = datetime.datetime.combine(
            end_date,
            datetime.time(23, 59, 59)
        )
        return self.order_by('timestamp').filter(
            timestamp__gte=start_time,
            timestamp__lte=end_time
        )


class HistoryModelBase(ModelBase):
    """ Model which keeps copies of other model's history

    Only copies the non-relational fields (no foreign keys, etc).

    """

    def __new__(cls, name, bases, attrs):
        if 'HistoryModel' in [b.__name__ for b in bases]:
            attr_meta = attrs.pop('Meta', None)
            original_model = getattr(attr_meta, 'original_model', None)

            if original_model is None and name.endswith('History'):
                base_class_name = name[:-7]
                original_model = get_class(
                    base_class_name,
                    attrs['__module__']
                )
            elif type(original_model) in [str, unicode]:
                if '.' in original_model:
                    original_model = get_model(
                        original_model.split('.')[0],
                        original_model.split('.')[1]
                    )
                else:
                    original_model = get_class(
                        attrs['__module__'],
                        original_model,
                    )

            if not original_model:
                raise ImproperlyConfigured(
                    'Could not find the original model for %s' % name
                )

            # Enumerate original model's fields
            original_fields = original_model._meta.fields
            field_list = getattr(attr_meta, 'history_track_fields', [])
            exclude_list = getattr(
                attr_meta,
                'history_exclude_fields',
                ['id']
            )

            # Build a list of fields to copy
            field_list = field_list or [
                f.name for f in original_fields if (not any([
                    f.name in exclude_list,
                    f.rel,
                    hasattr(f, 'auto_now') and f.auto_now,
                    hasattr(f, 'auto_now_add') and f.auto_now_add
                ]))
            ]

            attrs['_history_fields'] = field_list

            # Remember the cached fields if any
            cache_fields = getattr(attr_meta, 'cache_fields', {})

            # Copy all fields from the original
            for field in original_fields:
                if field.name in field_list:
                    attrs[field.name] = copy.deepcopy(field)

            # Add reference to the original
            attrs['_original'] = models.ForeignKey(
                original_model,
                related_name='history',
                editable=False,
            )

            # Add custom manager
            attrs['history'] = HistoryManager()

            # Get the final model
            model = ModelBase.__new__(cls, name, bases, attrs)

            # We must use ``weak=False`` here, because by default, Django uses
            # weak references to function which get garbage-collected before
            # we get a chance to fire the signals.
            @receiver(post_save, sender=original_model, weak=False)
            def record_history(sender, instance, **kwargs):
                """ Auto-create history in post-save signal from original model

                The receiver will look at the fields list generated during
                model creation, and also look into ``cache_fields`` meta
                property, in order to cache the user-specified cache fields.

                """

                instance_data = {'_original': instance}
                for field in field_list:
                    instance_data[field] = getattr(instance, field, None)
                for cache_field, cache_field_opts in cache_fields.items():
                    cache_object = getattr(instance, cache_field, None)

                    if hasattr(cache_field_opts, '__call__'):
                        instance_data[cache_field] = cache_field_opts(
                            cache_object
                        )
                    else:
                        if type(cache_field_opts) is dict:
                            local_field_name = cache_field_opts.get(
                                'local_field',
                                cache_field
                            )
                            object_field_name = cache_field_opts['object_field']
                        else:
                            local_field_name = cache_field
                            object_field_name = cache_field_opts
                        instance_data[local_field_name] = getattr(
                            cache_object,
                            object_field_name,
                            None
                        )
                history_object = model(**instance_data)
                history_object.save()

            return model
        else:
            # This is our HistoryModel class, so just return as usual
            return ModelBase.__new__(cls, name, bases, attrs)


class HistoryModel(Model):
    """ Base class for history models

    Subclass this to make your class history models.

    """

    __metaclass__ = HistoryModelBase

    timestamp = models.DateTimeField(auto_now_add=True, editable=False)

    @property
    def _original_model(self):
        return self._original.__class__

    @property
    def _original_model_name(self):
        return self._original_model.__name__

    def __unicode__(self):
        return "%s '%s' recorded at %s" % (
            self._original_model_name,
            self._original,
            self.timestamp
        )

    class Meta:
        abstract = True
