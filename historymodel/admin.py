from django.contrib import admin


class HistoryModelAdmin(admin.ModelAdmin):
    """ Template HistoryModel admin

    Subclass this admin class to manage your own history models.

    """

    list_display = (
        '_original',
        '_original_model_name',
        'timestamp',
    )
    list_filter = (
        'timestamp',
    )
